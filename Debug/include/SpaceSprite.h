#include "include/hgeanim.h"
#include "include/hge.h"
#include "include/hgerect.h"
struct Point
{
	float x;
	float y;
};
typedef WORD        SPRITEACTION;
const SPRITEACTION  SA_NONE      = 0x0000L,
                    SA_KILL      = 0x0001L,
                    SA_ADDSPRITE = 0x0002L;
typedef WORD        BOUNDSACTION;
const BOUNDSACTION  BA_STOP   = 0,
                    BA_WRAP   = 1,
                    BA_BOUNCE = 2,
                    BA_DIE    = 3;
class SpaceSprite:public hgeAnimation
{
public:
	Point m_ptVelocity;//速度
	Point v0;//左上角的顶点
	hgeRect m_Bound;//边界矩形
	hgeRect m_Position;//位置矩形
	BOUNDSACTION m_baBoundsAction;//边界碰撞动作
	//Kill();//清除子画面
	//CalcCollisionRec();//计算碰撞矩形
	//TestCollisionRec();//测试是否碰撞
	//virtual AddSprite();//增加子画面
	
	//hgeRect m_rcCollision;//碰撞矩形
	
	//bool m_bHidden;//是否隐藏
	//bool m_bDying;//是否删除
	SpaceSprite(HTEXTURE tex,int nframes,float FPS,float x,float y,float w,float h,float vx=0,float vy=0,float x0=0,float y0=0):hgeAnimation(tex,nframes,FPS,x,y,w,h)
	{
     m_ptVelocity.x=vx;
	 m_ptVelocity.y=vy;
	 v0.x=x0;
	 v0.y=y0;
	 m_Position.x1=x0;
	 m_Position.y1=y0;
	 m_Position.x2=x0+w;
	 m_Position.y2=y0+h;
	 m_Bound.x1=0;
	 m_Bound.y1=0;
	 m_Bound.x2=500;
	 m_Bound.y2=500;
	}
	//设置位置
	SetPosition(float x,float y)
	{
		v0.x=x;
		v0.y=y;
	}
	//移动位置
	OffsetPosition(float x,float y)
	{
		v0.x+=x;
		v0.y+=y;
	}
	//渲染
	Render()
	{
		hgeAnimation::Render(v0.x,v0.y);
	}
//在CPP中定义
	//子画面更新
    void  Update(float dt);
	
};