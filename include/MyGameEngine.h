#pragma comment(lib,"lib/hge.lib")
#pragma comment(lib,"lib/hgehelp.lib")
#pragma once
#include "hge.h"
#include "hgesprite.h"
#include "hgeanim.h"
#include "hgefont.h"
#include "hgedistort.h"
#include "SpaceSprite.h"
#include <vector>
using namespace std;

extern HGE* hge;
extern vector<SpaceSprite*> m_vSprites;

int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int);
void NewGame();
//子画面破坏时
void SpriteDying(SpaceSprite* pSpriteDying);
//处理键盘消失
bool HandleKeys(float dt);
//处理碰撞
BOOL SpriteCollision(SpaceSprite* pSpriteHitter, SpaceSprite* pSpriteHittee);
//检查所有的碰撞
BOOL CheckSpriteCollision(SpaceSprite* pTestSprite);
//增加子画面
void AddSprite(SpaceSprite* pSprite);
//渲染所有的子画面
void DrawSprites();
//更新所有的子画面
void UpdateSprites(float dt);
//清除所有的子画面
void CleanupSprites();
//初始化
void GameStart();
void GameEnd();
//渲染处理
bool RenderFunc();
//逻辑处理
bool FrameFunc();