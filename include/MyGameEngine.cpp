#include "MyGameEngine.h"
//清除所有的子画面
void CleanupSprites()
{
  // Delete and remove the sprites in the sprite vector
  vector<SpaceSprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
  {
    delete (*siSprite);
  }
}
//检查所有的碰撞
BOOL CheckSpriteCollision(SpaceSprite* pTestSprite)
{
  // See if the sprite has collided with any other sprites
  vector<SpaceSprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
  {
    // Make sure not to check for collision with itself
    if (pTestSprite == (*siSprite))
      continue;
    // Test the collision
    if (pTestSprite->TestCollision(*siSprite))
      // Collision detected
    return SpriteCollision((*siSprite), pTestSprite);
  }
  // No collision
  return FALSE;
}
//增加子画面
void AddSprite(SpaceSprite* pSprite)
{
	// Add a sprite to the sprite vector
  if (pSprite != NULL)
  {
    // See if there are sprites already in the sprite vector
    if (m_vSprites.size() > 0)
    {
      // Find a spot in the sprite vector to insert the sprite by its z-order
      vector<SpaceSprite*>::iterator siSprite;
      for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
        if (pSprite->GetZ() <(*siSprite)->GetZ())
        {
          // Insert the sprite into the sprite vector
          m_vSprites.insert(siSprite, pSprite);
          return;
        }
    }
    // The sprite's z-order is highest, so add it to the end of the vector
    m_vSprites.push_back(pSprite);
  }
}
//渲染所有的子画面
void DrawSprites()
{
  // Draw the sprites in the sprite vector
  vector<SpaceSprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
  {
	  if((*siSprite)->m_bHidden==false)
	  (*siSprite)->Render();
  }
}
//更新所有的子画面
void UpdateSprites(float dt)
{
//   MessageBox(NULL,"OK","ERROR",MB_OK);
  // Check to see if the sprite vector needs to grow
  if (m_vSprites.size() >= (m_vSprites.capacity() / 2))
    m_vSprites.reserve(m_vSprites.capacity() * 2);

  // Update the sprites in the sprite vector
  hgeRect          rcOldSpritePos;
  SPRITEACTION     saSpriteAction;
  vector<SpaceSprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
  {
    // Save the old sprite position in case we need to restore it
  //  rcOldSpritePos = (*siSprite)->m_Position;

    // Update the sprite
    saSpriteAction = (*siSprite)->Update(dt);

	
     // Handle the SA_ADDSPRITE sprite action
    if (saSpriteAction & SA_ADDSPRITE)
      // Allow the sprite to add its sprite
      AddSprite((*siSprite)->AddSprite());

    // Handle the SA_KILL sprite action
    if (saSpriteAction & SA_KILL)
    {
      // Notify the game that the sprite is dying
      SpriteDying(*siSprite);

      delete (*siSprite);
      m_vSprites.erase(siSprite);
      siSprite--;
      continue;
    }

	//检查其中一个与其他所有的碰撞
	CheckSpriteCollision(*siSprite);
#if 0
    if (CheckSpriteCollision(*siSprite))
	{
      // Restore the old sprite position
      (*siSprite)->SetPosition(rcOldSpritePos.x1,rcOldSpritePos.y1);
	}
#endif
  }
}
//入口函数
int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int)
{
	hge=hgeCreate(HGE_VERSION);
	hge->System_SetState(HGE_SCREENWIDTH,MYSCREENWIDTH);
	hge->System_SetState(HGE_SCREENHEIGHT,MYSCREENHEIGHT);
	hge->System_SetState(HGE_FRAMEFUNC,FrameFunc);
	hge->System_SetState(HGE_RENDERFUNC,RenderFunc);
	hge->System_SetState(HGE_FPS,70);
	hge->System_SetState(HGE_TITLE,"Space");
	hge->System_SetState(HGE_WINDOWED,true);
	hge->System_SetState(HGE_USESOUND,true);
	hge->System_SetState(HGE_ZBUFFER,true);
	hge->System_SetState(HGE_HIDEMOUSE,false);
	if(hge->System_Initiate())
	{
	 GameStart();
	}
	GameEnd();
	return 0;
}
