#ifndef SPACESPRITE
#define SPACESPRITE
#include "hgeanim.h"
#include "hge.h"
#include "hgerect.h"
#include "Const.h" 
extern HTEXTURE minetex;
class SpaceSprite:public hgeAnimation
{
public:
	int iLive;//生命
	int iPower;//子弹
	int iLevel;//等级
	Point m_ptVelocity;//速度
	Point v0;//左上角的顶点
	hgeRect m_Bound;//边界矩形
	hgeRect m_Position;//位置矩形
	BOUNDSACTION m_baBoundsAction;//边界碰撞动作
	hgeRect m_rcCollision;//碰撞矩形
	bool m_bOneCycle;//只循环一个周期
	//清除子画面
	void Kill()
	{
		m_bDying=true;
	}
	//计算碰撞矩形
	CalcCollisionRec()
	{
		if(GetTexture()==minetex)//核弹的矩形很大
		{
		if(rand()%5==0)
		{
		int iXShrink =2*(m_Position.x2- m_Position.x1);
		int iYShrink =2*(m_Position.y2- m_Position.y1);
		m_rcCollision.Set(m_Position.x1-(rand()%100),m_Position.y1-iYShrink-(rand()%100),m_Position.x2+iXShrink+(rand()%100),
		m_Position.y2+iYShrink+(rand()%100));
		}
		}
			else
		{
		int iXShrink = (m_Position.x2- m_Position.x1) / 12;
		int iYShrink = (m_Position.y2- m_Position.y1) / 12;
		m_rcCollision.Set(m_Position.x1+iXShrink,m_Position.y1+iYShrink,m_Position.x2-iXShrink,
		m_Position.y2-iYShrink);
		}
	}
	//测试是否碰撞
	bool TestCollision(SpaceSprite* pTestSprite)
	{
		CalcCollisionRec();
		pTestSprite->CalcCollisionRec();
		hgeRect rcTest=pTestSprite->m_rcCollision;
        return m_rcCollision.x1 <= rcTest.x2 &&
         rcTest.x1 <= m_rcCollision.x2 &&
         m_rcCollision.y1 <= rcTest.y2 &&
         rcTest.y1 <= m_rcCollision.y2;
	}
	//增加子画面
	virtual SpaceSprite* AddSprite()
	{
		return NULL;
	}
	bool m_bHidden;//是否隐藏
	bool m_bDying;//是否删除
	//初始化
	SpaceSprite(HTEXTURE tex,int nframes,float FPS,float x,float y,float w,float h,float iX=300,float iY=400):hgeAnimation(tex,nframes,FPS,x,y,w,h)
	{
	 m_bHidden=false;
	 iPower=1;	 
	 iLevel=0;
	 iLive=100;
	 m_bOneCycle=false;
     m_ptVelocity.x=0;
	 m_ptVelocity.y=0;
	 v0.x=iX;
	 v0.y=iY;
	 m_Position.x1=iX;
	 m_Position.y1=iY;
	 m_Position.x2=iX+w;
	 m_Position.y2=iY+h;
	 m_Bound.x1=0;
	 m_Bound.y1=0;
	 m_Bound.x2=MYSCREENWIDTH-30;
	 m_Bound.y2=MYSCREENHEIGHT-30;
	 m_bDying=false;
	 m_baBoundsAction=BA_DIE;
	 SetZ(0.5);
	}
	//设置位置
	SetPosition(float x,float y)
	{
		v0.x=x;
		v0.y=y;
		m_Position.x1=v0.x;
	    m_Position.y1=v0.y;
	    m_Position.x2=v0.x+GetWidth();
	    m_Position.y2=v0.y+GetHeight();
	}
	//设置速度
	SetVelocity(float x,float y)
	{
     m_ptVelocity.x=x;
	 m_ptVelocity.y=y;
	}
	SetVelocity(Point p)
	{
     m_ptVelocity.x=p.x;
	 m_ptVelocity.y=p.y;
	}
	SetPosition(Point p)
	{
        v0.x=p.x;
		v0.y=p.y;
		m_Position.x1=v0.x;
	    m_Position.y1=v0.y;
	    m_Position.x2=v0.x+GetWidth();
	    m_Position.y2=v0.y+GetHeight();
	}
	//移动位置
	OffsetPosition(float x,float y)
	{
		v0.x+=x;
		v0.y+=y;
		m_Position.x1=v0.x;
	    m_Position.y1=v0.y;
	    m_Position.x2=v0.x+GetWidth();
	    m_Position.y2=v0.y+GetHeight();
	}
	//渲染
	Render()
	{
		hgeAnimation::Render(v0.x,v0.y);
	}
//在CPP中定义
	//子画面更新
    virtual SPRITEACTION  Update(float dt);
};
#endif