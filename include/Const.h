#ifdef CONST 
#define CONST
#include <math.h>
const int MYSCREENWIDTH=600;
const int MYSCREENHEIGHT=500;
const int DDELAY=5;
class Point
{
public:
	float x;
	float y;
};
typedef WORD        SPRITEACTION;
const SPRITEACTION  SA_NONE      = 0x0000L,
                    SA_KILL      = 0x0001L,
                    SA_ADDSPRITE = 0x0002L;
typedef WORD        BOUNDSACTION;
const BOUNDSACTION  BA_STOP   = 0,
                    BA_WRAP   = 1,
                    BA_BOUNCE = 2,
                    BA_DIE    = 3;
#endif
