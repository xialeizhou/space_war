#include "SpaceSprite.h"  
//子画面更新
SPRITEACTION SpaceSprite::Update(float dt)
{
  if(m_bOneCycle)
  {
	  if(GetFrame()==GetFrames()-1)
	     m_bDying=true;
  }
  if(iLive<=0)
	  m_bDying=true;
  if (m_bDying)
    return SA_KILL;

  hgeAnimation::Update(dt);
  
  // Update the position
  POINT ptNewPosition, ptSpriteSize, ptBoundsSize;
  ptNewPosition.x = m_Position.x1 + m_ptVelocity.x;
  ptNewPosition.y = m_Position.y1 + m_ptVelocity.y;
  ptSpriteSize.x = m_Position.x2 - m_Position.x1;
  ptSpriteSize.y = m_Position.y2 - m_Position.y1;
  ptBoundsSize.x = m_Bound.x2 - m_Bound.x1;
  ptBoundsSize.y = m_Bound.y2 - m_Bound.y1;

  // Check the bounds
  // Wrap?
  if (m_baBoundsAction == BA_WRAP)
  {
    if ((ptNewPosition.x + ptSpriteSize.x) < m_Bound.x1)
      ptNewPosition.x = m_Bound.x2;
    else if (ptNewPosition.x > m_Bound.x2)
      ptNewPosition.x = m_Bound.x1 - ptSpriteSize.x;
    if ((ptNewPosition.y + ptSpriteSize.y) < m_Bound.y1)
      ptNewPosition.y = m_Bound.y2;
    else if (ptNewPosition.y > m_Bound.y2)
      ptNewPosition.y = m_Bound.y1 - ptSpriteSize.y;
  }
  // Bounce?
  else if (m_baBoundsAction == BA_BOUNCE)
  {
    BOOL bBounce = FALSE;
    POINT ptNewVelocity;
	ptNewVelocity.x=m_ptVelocity.x;
    ptNewVelocity.y=m_ptVelocity.y;
    if (ptNewPosition.x < m_Bound.x1)
    {
      bBounce = TRUE;
      ptNewPosition.x = m_Bound.x1;
      ptNewVelocity.x = -ptNewVelocity.x;
    }
    else if ((ptNewPosition.x + ptSpriteSize.x) > m_Bound.x2)
    {
      bBounce = TRUE;
      ptNewPosition.x = m_Bound.x2 - ptSpriteSize.x;
      ptNewVelocity.x = -ptNewVelocity.x;
    }
    if (ptNewPosition.y < m_Bound.y1)
    {
      bBounce = TRUE;
      ptNewPosition.y = m_Bound.y1;
      ptNewVelocity.y = -ptNewVelocity.y;
    }
    else if ((ptNewPosition.y + ptSpriteSize.y) > m_Bound.y2)
    {
      bBounce = TRUE;
      ptNewPosition.y = m_Bound.y2 - ptSpriteSize.y;
      ptNewVelocity.y = -ptNewVelocity.y;
    }
    if (bBounce)
      SetVelocity(ptNewVelocity.x,ptNewVelocity.y);
  }
  // Die?
  else if (m_baBoundsAction == BA_DIE)
  {
    if ((ptNewPosition.x + ptSpriteSize.x) < m_Bound.x1 ||
      ptNewPosition.x > m_Bound.x2 ||
      (ptNewPosition.y + ptSpriteSize.y) < m_Bound.y1 ||
      ptNewPosition.y > m_Bound.y2)
      return SA_KILL;
  }
  // Stop (default)
  else
  {
	//左边
    if (ptNewPosition.x< m_Bound.x1)
    {
      ptNewPosition.x=0;
      SetVelocity(0, 0);
    }
	//右边
     if(ptNewPosition.x>(m_Bound.x2-ptSpriteSize.x))
    {
      ptNewPosition.x=m_Bound.x2-ptSpriteSize.x;
      SetVelocity(0, 0);
    }
	//上边
    if (ptNewPosition.y<m_Bound.y1)
    {
      ptNewPosition.y=m_Bound.y1;
      SetVelocity(0, 0);
    }
	//下边
    if (ptNewPosition.y>(m_Bound.y2 - ptSpriteSize.y))
    {
      ptNewPosition.y=m_Bound.y2 - ptSpriteSize.y;
      SetVelocity(0, 0);
    }
  }
  SetPosition(ptNewPosition.x,ptNewPosition.y);
  return SA_NONE;
}