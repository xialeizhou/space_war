#pragma once
#include "Const.h"
#include "MyGameEngine.h"
#include "SpaceSprite.h"
#include "AlienSprite.h"
#include "hgeparticle.h"
#include "hgeguictrls.h"
#include "menuitem.h"
void AddAlien();
//系统
static int i_Dif=0;
int g_iDifficulty;
int g_iShotLevel;
int g_iMoney;
//控件
hgeGUI				*gui;
HEFFECT				snd;
//加载资源
HGE* hge=NULL;
SpaceSprite* g_EnemyShot;
SpaceSprite* g_Ship;
SpaceSprite* g_ShipShot;
vector<SpaceSprite*> g_Enemys;
vector<SpaceSprite*> m_vSprites;
hgeFont* fnt;//字体
hgeSprite* bgspr;//背景
hgeSprite*			shipparsprite;
//粒子系统
    //奖励
    hgeParticleSystem*	shippar;
    hgeParticleSystem*  shipbonuspar0;
    hgeParticleSystem*  shipbonuspar1;
    hgeParticleSystem*  shipbonuspar2;
    hgeParticleSystem*  shipbonuspar3;
	//碰撞
    hgeParticleSystem*  shipcolpar0;
    hgeParticleSystem*  shipcolpar1;

//图片资源
HTEXTURE minetex;
HTEXTURE shellplaytex;//闪电
HTEXTURE bgtex;//背景
HTEXTURE shippartex;//背景
HTEXTURE shiptex;//我方
HTEXTURE ship1tex;
HTEXTURE ship2tex;
HTEXTURE ship3tex;
HTEXTURE ship4tex;
HTEXTURE ship5tex;

HTEXTURE shipshottex;//我方子弹0
HTEXTURE shipshot1tex;//我方子弹1
HTEXTURE shipshot2tex;//我方子弹2
HTEXTURE shipshot3tex;//我方子弹2

HTEXTURE boomtex;//爆炸
HTEXTURE enemy0tex;//敌人零
HTEXTURE enemy1tex;//敌人一
HTEXTURE enemy2tex;//敌人二
HTEXTURE enemy3tex;//敌人三
HTEXTURE enemy4tex;//敌人四
HTEXTURE enemy5tex;//敌人五
HTEXTURE enemy6tex;//敌人六
HTEXTURE enemyshot1tex;//敌人子弹一
HTEXTURE enemyshot2tex;//敌人子弹二
HTEXTURE enemyshot3tex;//敌人子弹三
HTEXTURE enemyshot4tex;//敌人子弹四
HTEXTURE roid0tex;//陨石零
HTEXTURE roid1tex;//陨石一
HTEXTURE roid2tex;//陨石二
hgeSprite* bonuspar0sprite;//奖励零
hgeSprite* bonuspar1sprite;//奖励一
hgeSprite* bonuspar2sprite;//奖励二
hgeSprite* bonuspar3sprite;//奖励三
HTEXTURE bonus0tex;//奖励零
HTEXTURE bonus1tex;//奖励一
HTEXTURE bonus2tex;//奖励二
HTEXTURE bonus3tex;//奖励三
HTEXTURE cloud0tex;//云
HTEXTURE cloud1tex;//云
HTEXTURE cloud2tex;//云
HTEXTURE cloud3tex;//云
HTEXTURE cloud4tex;//云
HTEXTURE cloud5tex;//云
HTEXTURE cloud6tex;//云
HTEXTURE cloud7tex;//云
HTEXTURE cloud8tex;//云
HTEXTURE cloud9tex;//云




