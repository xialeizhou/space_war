#pragma once 
#include "SpaceSprite.h"
extern int g_iDifficulty;
extern HTEXTURE enemy0tex;//敌人零
extern HTEXTURE enemy1tex;//敌人一
extern HTEXTURE enemy2tex;//敌人二
extern HTEXTURE enemy3tex;//敌人三
extern HTEXTURE enemy4tex;//敌人四
extern HTEXTURE enemy5tex;//敌人五
extern HTEXTURE enemy6tex;//敌人六

extern HTEXTURE enemyshot1tex;//敌人子弹一
extern HTEXTURE enemyshot2tex;//敌人子弹二
extern HTEXTURE enemyshot3tex;//敌人子弹三
extern HTEXTURE enemyshot4tex;//敌人子弹四
class AlienSprite : public SpaceSprite
{
public:
  AlienSprite(HTEXTURE tex,int nframes,float FPS,float x,float y,float w,float h,float vx=0,float vy=0,float x0=0,float y0=0)
	  :SpaceSprite(tex,nframes,FPS,x,y,w,h,x0,y0)
	{
	  /*
     m_ptVelocity.x=vx;
	 m_ptVelocity.y=vy;
	 v0.x=x0;
	 v0.y=y0;
	 m_Position.x1=x0;
	 m_Position.y1=y0;
	 m_Position.x2=x0+w;
	 m_Position.y2=y0+h;
	 m_Bound.x1=0;
	 m_Bound.y1=0;
	 m_Bound.x2=SCREENWIDTH;
	 m_Bound.y2=SCREENHEIGHT;
	 m_bDying=false;
	 m_baBoundsAction=BA_BOUNCE;
	 SetZ(0.5);
	 */
	 m_baBoundsAction=BA_DIE;
	 iLive=60;
	}
  virtual SPRITEACTION  Update(float dt);
  virtual SpaceSprite*  AddSprite();
};