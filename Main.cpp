#include "include/Main.h"
//新游戏开始  
void NewGame()
{
	
}
void AddCloud()
{
/*
SpaceSprite* pSprite;
switch((rand()%10)+2)
{
case 0://黑云
pSprite=new SpaceSprite(cloud0tex,1,5,0,0,256,128,50,50);
pSprite->SetVelocity((rand()%7)+1,(rand()%2)+1);
//;
pSprite->SetPosition((rand()%400),(rand()%250)+10);
pSprite->SetBlendMode(BLEND_COLORMUL);
pSprite->m_baBoundsAction=BA_DIE;
AddSprite(pSprite);break;
case 1://黑云
pSprite=new SpaceSprite(cloud1tex,1,5,0,0,256,200,50,50);
pSprite->SetVelocity((rand()%7)-1,(rand()%2)-5);
pSprite->SetBlendMode(BLEND_COLORMUL);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),(rand()%250)+10);
AddSprite(pSprite);break; 
case 2:
pSprite=new SpaceSprite(cloud2tex,1,5,0,0,94,126,50,50);
pSprite->SetVelocity((rand()%7)-5,(rand()%2)+5);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),10);
AddSprite(pSprite);break; 
case 3:
pSprite=new SpaceSprite(cloud3tex,1,5,0,0,208,108,50,50);
pSprite->SetVelocity((rand()%7)-6,(rand()%2)+2);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),(rand()%10)+10);
AddSprite(pSprite);break; 
case 4:
pSprite=new SpaceSprite(cloud4tex,1,5,0,0,227,119,50,50);
pSprite->SetVelocity(-(rand()%7)-2,(rand()%2)+1);
pSprite->SetPosition((rand()%400),(rand()%10)+10);
pSprite->m_baBoundsAction=BA_DIE;
AddSprite(pSprite);break; 
case 5:
pSprite=new SpaceSprite(cloud5tex,1,5,0,0,243,94,50,50);
pSprite->SetVelocity((rand()%7)+3,(rand()%2)+2);
pSprite->SetPosition((rand()%400),(rand()%4)+10);
pSprite->m_baBoundsAction=BA_DIE;
AddSprite(pSprite);break; 
case 6:
pSprite=new SpaceSprite(cloud6tex,1,5,0,0,258,99,50,50);
pSprite->SetVelocity((rand()%7)+3,(rand()%2)+5);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),(rand()%10)+10);
AddSprite(pSprite);break; 
case 7:
pSprite=new SpaceSprite(cloud7tex,1,5,0,0,147,65,50,50);
pSprite->SetVelocity((rand()%7)-8,(rand()%2)+2);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),(rand()%3)+10);
AddSprite(pSprite);break; 
case 8:
pSprite=new SpaceSprite(cloud8tex,1,5,0,0,183,85,50,50);
pSprite->SetVelocity((rand()%7)-6,(rand()%2)+4);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),(rand()%5)+10);
AddSprite(pSprite);break; 
case 9:
pSprite=new SpaceSprite(cloud9tex,1,5,0,0,257,145,50,50);
pSprite->SetVelocity((rand()%7)+3,(rand()%2)+6);
pSprite->m_baBoundsAction=BA_DIE;
pSprite->SetPosition((rand()%400),(rand()%2)+10);
AddSprite(pSprite);break; 
}
	*/
}
//增加奖励
void AddBonus(float x,float y)
{
    SpaceSprite* pSprite;
	switch(rand()%4)
	{
	case 0:
		pSprite=new SpaceSprite(bonus0tex,32,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetVelocity(0,1);
		pSprite->SetPosition(x,y);
		AddSprite(pSprite);break;
    case 1:
		pSprite=new SpaceSprite(bonus1tex,32,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetMode(HGEANIM_PINGPONG);
		pSprite->SetVelocity(0,2);
		pSprite->SetPosition(x,y);
		AddSprite(pSprite);break;
	case 2:
		pSprite=new SpaceSprite(bonus2tex,32,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetVelocity(0,3);
		pSprite->SetPosition(x,y);
		AddSprite(pSprite);
		break;
    case 3:
		pSprite=new SpaceSprite(bonus3tex,8,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetVelocity(0,1);
		pSprite->SetPosition(x,y);
		AddSprite(pSprite);break;
	}
}
void AddBonus()
{
	SpaceSprite* pSprite;
	switch(rand()%4)
	{
	case 0:
		
		pSprite=new SpaceSprite(bonus0tex,32,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetVelocity(0,3);
		pSprite->SetPosition((rand()%400),(rand()%250)+10);
		AddSprite(pSprite);break;
    case 1:
		pSprite=new SpaceSprite(bonus1tex,32,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetMode(HGEANIM_PINGPONG);
		pSprite->SetVelocity(0,5);
		pSprite->SetPosition((rand()%400),(rand()%250)+10);
		AddSprite(pSprite);break;
	case 2:
	/*
	pSprite=new SpaceSprite(bonus2tex,32,5,0,0,32,32,50,50);
	pSprite->Play();
	pSprite->SetVelocity(0,4);
	pSprite->SetPosition((rand()%400),(rand()%250)+10);
	AddSprite(pSprite);
		*/
		break;
    case 3:
		pSprite=new SpaceSprite(bonus3tex,8,5,0,0,32,32,50,50);
		pSprite->Play();
		pSprite->SetVelocity(0,3);
		pSprite->SetPosition((rand()%400),(rand()%250)+10);
		AddSprite(pSprite);break;
	}
}
void AddRoid()
{
	SpaceSprite* pSprite;
	static int iRoid=0;
	if(++iRoid>10)
	{
		iRoid=0;
		switch(rand()%3)
		{
		case 0:
			pSprite=new SpaceSprite(roid0tex,30,5,0,0,64,64,50,50);
			pSprite->Play();
			if(rand()%g_iDifficulty==0)
			pSprite->SetVelocity(0,5);	
				else
				{
					if(rand()%2==0)
			       pSprite->SetVelocity(0,1);
                   else
			       pSprite->SetVelocity(0,3);
				}
			pSprite->m_baBoundsAction=BA_DIE;
			pSprite->SetPosition(g_Ship->v0.x,0);
			pSprite->iLive=150-g_iDifficulty+rand()%500;
			AddSprite(pSprite);break;
		case 1:
			pSprite=new SpaceSprite(roid1tex,30,5,0,0,96,96,50,50);
			pSprite->Play();
			pSprite->iLive=150-g_iDifficulty+200+rand()%500;
			pSprite->m_baBoundsAction=BA_DIE;
			pSprite->SetMode(HGEANIM_PINGPONG);
			if(rand()%g_iDifficulty==0)
			pSprite->SetVelocity(0,6);	
				else
				{
					if(rand()%2==0)
			       pSprite->SetVelocity(0,1);
                   else
			       pSprite->SetVelocity(0,4);
				}
			pSprite->SetPosition(g_Ship->v0.x,0);
			AddSprite(pSprite);break;
		case 2:
			pSprite=new SpaceSprite(roid2tex,36,5,0,0,128,128,50,50);
			pSprite->Play();
			pSprite->iLive=150-g_iDifficulty+200+rand()%300;
			if(rand()%g_iDifficulty==0)
			pSprite->SetVelocity(0,5);	
				else
				{
                   if(rand()%2==0)
			       pSprite->SetVelocity(0,1);
                   else
			       pSprite->SetVelocity(0,3);
				}
			pSprite->m_baBoundsAction=BA_DIE;
			pSprite->SetPosition(g_Ship->v0.x,0);
			AddSprite(pSprite);break;
		}
	}
}
void AddAlien()
{
	AlienSprite* pSprite;
	switch(rand()%7)
	{
	case 0:
		pSprite=new AlienSprite(enemy0tex,8,5,0,0,64,64,50,50);
		pSprite->Play();
		pSprite->iLive=30+rand()%10;
		pSprite->SetVelocity(1,rand()%2);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	case 1:
		pSprite=new AlienSprite(enemy1tex,8,5,0,0,64,64,50,50);
		pSprite->Play();
		pSprite->iLive=40+rand()%20;
		pSprite->SetVelocity(-1,rand()%3);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	case 2:
		pSprite=new AlienSprite(enemy2tex,8,5,0,0,64,64,50,50);
		pSprite->Play();
		pSprite->iLive=50+rand()%10;
		pSprite->SetVelocity(1,rand()%3);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	case 3:
		pSprite=new AlienSprite(enemy3tex,8,5,0,0,64,64,50,50);
		pSprite->Play();
		pSprite->iLive=50+rand()%20;
		pSprite->SetVelocity(-1,rand()%3);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	case 4:
		pSprite=new AlienSprite(enemy4tex,8,5,0,0,64,64,50,50);
		pSprite->Play();
		pSprite->iLive=60+rand()%20;
		pSprite->SetVelocity(-1,rand()%3);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	case 5:
		pSprite=new AlienSprite(enemy5tex,4,6,0,0,48,62,50,50);
		pSprite->Play();
		pSprite->iLive=50+rand()%20;
		pSprite->SetVelocity(1,rand()%3);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	case 6:
		pSprite=new AlienSprite(enemy6tex,8,5,0,0,64,64,50,50);
		pSprite->Play();
		pSprite->iLive=20+rand()%30;
		pSprite->SetVelocity(-1,rand()%3);
		pSprite->SetPosition((rand()%400),0);
		AddSprite(pSprite);break;
	}
}
//子画面破坏时
void SpriteDying(SpaceSprite* pSpriteDying)
{
	// See if an alien missile sprite is dying
	if (pSpriteDying->GetTexture()==shipshottex)
	{ 
		
	}
}
//处理键盘消息
bool HandleKeys(float dt)
{
	switch(hge->Input_GetKey())
	{
	case HGEK_ESCAPE:
		return true;
	case HGEK_UP:
		{
			g_Ship->OffsetPosition(0,-10);
			if(g_Ship->GetTexture()==shiptex)
				g_Ship->SetFrame(0);
			break;
		}
	case HGEK_DOWN:
		{
			g_Ship->OffsetPosition(0,10);
			if(g_Ship->GetTexture()==shiptex)
				g_Ship->SetFrame(1);break;
		}
	case HGEK_LEFT:
		{
			g_Ship->OffsetPosition(-10,0);
			if(g_Ship->GetTexture()==shiptex)
				g_Ship->SetFrame(2);break;
		}
	case HGEK_RIGHT:
		{
			g_Ship->OffsetPosition(10,0);
			if(g_Ship->GetTexture()==shiptex)
				g_Ship->SetFrame(3);break;
		}
	case HGEK_SPACE:
		{
			if(g_iMoney<=100)
			{
				if(g_iShotLevel==0)//两发
				{
					SpaceSprite* pSprite = new SpaceSprite(shipshottex,6,5,0,0,20,20);
					pSprite->SetPosition(g_Ship->v0.x+5,g_Ship->v0.y+20);
					int v=hge->Random_Int(7,8);
					pSprite->SetVelocity(0,-v);
					pSprite->SetBlendMode(BLEND_COLORMUL);
					pSprite->m_baBoundsAction=BA_DIE;
					pSprite->Play();
					AddSprite(pSprite);
					SpaceSprite* pSprite2 = new SpaceSprite(shipshottex,6,20,0,0,20,20);
					pSprite2->SetPosition(g_Ship->v0.x+40,g_Ship->v0.y+20);
					pSprite2->SetVelocity(0,-v);
					pSprite2->SetBlendMode(BLEND_COLORMUL);
					pSprite2->Play();
					pSprite2->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite2);
				}
				if(g_iShotLevel==1)//三发
				{
					//左边子弹
					SpaceSprite* pSprite = new SpaceSprite(shipshottex,6,5,0,0,20,20);
					pSprite->SetPosition(g_Ship->v0.x,g_Ship->v0.y+30);
					int v=hge->Random_Int(7,8);
					pSprite->SetVelocity(0,-v);
					pSprite->SetBlendMode(BLEND_COLORMUL);
					pSprite->m_baBoundsAction=BA_DIE;
					pSprite->Play();
					AddSprite(pSprite);
					//右边子弹
					SpaceSprite* pSprite2 = new SpaceSprite(shipshottex,6,20,0,0,20,20);
					pSprite2->SetPosition(g_Ship->v0.x+45,g_Ship->v0.y+30);
					pSprite2->SetVelocity(0,-v);
					pSprite2->SetBlendMode(BLEND_COLORMUL);
					pSprite2->Play();
					pSprite2->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite2);
					//中间子弹
					SpaceSprite* pSprite6 = new SpaceSprite(shipshot2tex,1,0,0,0,40,40);
					pSprite6->SetPosition(g_Ship->v0.x+12,g_Ship->v0.y);
					pSprite6->SetVelocity(0,-8);
					pSprite6->SetBlendMode(BLEND_COLORMUL|BLEND_ZWRITE);
					pSprite6->m_baBoundsAction=BA_DIE;
					pSprite6->Play();
					AddSprite(pSprite6);
				}
				if(g_iShotLevel==2)//四发
				{
					//左边1子弹
					SpaceSprite* pSprite = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite->SetPosition(g_Ship->v0.x,g_Ship->v0.y+30);
					int v=hge->Random_Int(7,8);
					pSprite->SetVelocity(-v,-v);
					pSprite->SetBlendMode(BLEND_COLORMUL);
					pSprite->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite);
					//右边1子弹
					SpaceSprite* pSprite2 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite2->SetPosition(g_Ship->v0.x+30,g_Ship->v0.y+30);
					pSprite2->SetVelocity(v,-v);
					pSprite2->SetBlendMode(BLEND_COLORMUL);
					pSprite2->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite2);
					//左边2子弹
					SpaceSprite* pSprite3 = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite3->SetPosition(g_Ship->v0.x+10,g_Ship->v0.y+2);
					pSprite3->SetVelocity(-v+2,-v-5);
					pSprite3->SetBlendMode(BLEND_COLORMUL);
					pSprite3->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite3);
					//右边2子弹
					SpaceSprite* pSprite4 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite4->SetPosition(g_Ship->v0.x+20,g_Ship->v0.y+2);
					pSprite4->SetVelocity(v-2,-v-5);
					pSprite4->SetBlendMode(BLEND_COLORMUL);
					pSprite4->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite4);
				}
				if(g_iShotLevel==3)//五发
				{
					//左边1子弹
					SpaceSprite* pSprite = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite->SetPosition(g_Ship->v0.x,g_Ship->v0.y+30);
					int v=hge->Random_Int(7,8);
					pSprite->SetVelocity(-v,-v);
					pSprite->SetBlendMode(BLEND_COLORMUL);
					pSprite->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite);
					//右边1子弹
					SpaceSprite* pSprite2 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite2->SetPosition(g_Ship->v0.x+30,g_Ship->v0.y+30);
					pSprite2->SetVelocity(v,-v);
					pSprite2->SetBlendMode(BLEND_COLORMUL);
					pSprite2->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite2);
					//左边2子弹
					SpaceSprite* pSprite3 = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite3->SetPosition(g_Ship->v0.x+10,g_Ship->v0.y+2);
					pSprite3->SetVelocity(-v+2,-v-5);
					pSprite3->SetBlendMode(BLEND_COLORMUL);
					pSprite3->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite3);
					//右边2子弹
					SpaceSprite* pSprite4 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite4->SetPosition(g_Ship->v0.x+20,g_Ship->v0.y+2);
					pSprite4->SetVelocity(v-2,-v-5);
					pSprite4->SetBlendMode(BLEND_COLORMUL);
					pSprite4->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite4);
					//中间子弹
					SpaceSprite* pSprite5 = new SpaceSprite(shipshot2tex,1,0,0,0,40,40);
					pSprite5->SetPosition(g_Ship->v0.x+12,g_Ship->v0.y);
					pSprite5->SetVelocity(0,-8);
					pSprite5->SetBlendMode(BLEND_COLORMUL|BLEND_ZWRITE);
					pSprite5->m_baBoundsAction=BA_DIE;
					pSprite5->Play();
					AddSprite(pSprite5);
				}
				if(g_iShotLevel==4)//六发
				{
					//左边1子弹
					SpaceSprite* pSprite = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite->SetPosition(g_Ship->v0.x,g_Ship->v0.y+30);
					int v=hge->Random_Int(7,8);
					pSprite->SetVelocity(-v,-v);
					pSprite->SetBlendMode(BLEND_COLORMUL);
					pSprite->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite);
					//右边1子弹
					SpaceSprite* pSprite2 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite2->SetPosition(g_Ship->v0.x+30,g_Ship->v0.y+30);
					pSprite2->SetVelocity(v,-v);
					pSprite2->SetBlendMode(BLEND_COLORMUL);
					pSprite2->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite2);
					//左边2子弹
					SpaceSprite* pSprite3 = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite3->SetPosition(g_Ship->v0.x+10,g_Ship->v0.y+2);
					pSprite3->SetVelocity(-v+1,-v-5);
					pSprite3->SetBlendMode(BLEND_COLORMUL);
					pSprite3->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite3);
					//右边2子弹
					SpaceSprite* pSprite4 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite4->SetPosition(g_Ship->v0.x+20,g_Ship->v0.y+2);
					pSprite4->SetVelocity(v-1,-v-5);
					pSprite4->SetBlendMode(BLEND_COLORMUL);
					pSprite4->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite4);
					//左边3子弹
					SpaceSprite* pSprite5 = new SpaceSprite(shipshot3tex,1,5,0,0,40,40);
					pSprite5->SetPosition(g_Ship->v0.x+5,g_Ship->v0.y+2);
					pSprite5->SetVelocity(-v+4,-v-9);
					pSprite5->SetBlendMode(BLEND_COLORMUL);
					pSprite5->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite5);
					//右边3子弹
					SpaceSprite* pSprite6 = new SpaceSprite(shipshot3tex,1,20,0,0,40,40);
					pSprite6->SetPosition(g_Ship->v0.x+30,g_Ship->v0.y+2);
					pSprite6->SetVelocity(v-4,-v-9);
					pSprite6->SetBlendMode(BLEND_COLORMUL);
					pSprite6->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite6);
				}
                if(g_iShotLevel==5)
				{
					SpaceSprite* pSprite6 = new SpaceSprite(minetex,1,0,0,0,64,64);
					pSprite6->SetPosition(g_Ship->v0.x+3,g_Ship->v0.y);
					pSprite6->SetVelocity(0,-1);
					pSprite6->SetBlendMode(BLEND_COLORMUL|BLEND_ZWRITE);
					pSprite6->m_baBoundsAction=BA_DIE;
					AddSprite(pSprite6);
				}
		}
		else
		{
			g_iMoney=0;			
			SpaceSprite* pSprite6 = new SpaceSprite(minetex,1,0,0,0,64,64);
			pSprite6->SetPosition(g_Ship->v0.x+3,g_Ship->v0.y);
			pSprite6->SetVelocity(0,-1);
			pSprite6->SetBlendMode(BLEND_COLORMUL|BLEND_ZWRITE);
			pSprite6->m_baBoundsAction=BA_DIE;
			AddSprite(pSprite6);
		}
		}
		break;
	}
	return false;
}
//处理碰撞
BOOL SpriteCollision(SpaceSprite* pSpriteHitter, SpaceSprite* pSpriteHittee)
{
	HTEXTURE pHitter=pSpriteHitter->GetTexture();
	HTEXTURE pHittee=pSpriteHittee->GetTexture();
	static int cur_power=1;
	if(
		//我方子弹的效果		
		(
		(
		pHitter==shipshottex||pHitter==shipshot1tex||pHitter==shipshot2tex||pHitter==shipshot3tex
		)
		&&
		(
		pHittee==enemy0tex||pHittee==enemy1tex||pHittee==enemy2tex||pHittee==enemy3tex||pHittee==enemy4tex||pHittee==enemy5tex||pHittee==enemy6tex
		||pHittee==roid0tex||pHittee==roid1tex||pHittee==roid2tex
		)
		)||(
		(
		pHitter==enemyshot1tex||pHitter==enemyshot2tex||pHitter==enemyshot3tex||pHitter==enemyshot4tex
		||pHitter==enemy0tex||pHitter==enemy1tex||pHitter==enemy2tex||pHitter==enemy3tex||pHitter==enemy4tex||pHitter==enemy5tex||pHitter==enemy6tex
		||pHitter==roid0tex||pHitter==roid1tex||pHitter==roid2tex
		)
		&&(pHittee==shiptex||pHittee==ship1tex||pHittee==ship2tex||pHittee==ship3tex||pHittee==ship4tex||pHittee==ship5tex)
		)
		)	  
	{ 
		if((pSpriteHittee->iLive)>1)
		{	
			(pSpriteHittee->iLive)--;
			shipcolpar0->MoveTo(pSpriteHittee->v0.x+(rand()%(int)(pSpriteHittee->GetWidth())),pSpriteHittee->v0.y+(rand()%(int)(pSpriteHittee->GetHeight())));
			shipcolpar0->info.nEmission=50;
			shipcolpar0->info.colColorStart=ARGB(255,255,200,0);
			//shipcolpar0->info.fSpread=0;
			shipcolpar0->info.fSizeStart=1;
			shipcolpar0->info.fSpeedMin=-100;
			shipcolpar0->info.fSpeedMax=-200;
			shipcolpar0->info.fSpread=0;
			shipcolpar0->info.fDirection=M_PI;
			//shipcolpar0->info.fParticleLifeMin=2;
			//shipcolpar0->info.fDirection=270;
			//shipcolpar0->info.fSpeedMin=-200;
			shipcolpar0->Fire();
		}
		else
		{
			//小型爆炸
			cur_power++;
			shipcolpar1->MoveTo(pSpriteHittee->v0.x+30,pSpriteHittee->v0.y+35);
			shipcolpar1->info.nEmission=50;
			shipcolpar1->info.colColorStart=ARGB(255,255,200,0);
			shipcolpar1->info.fSpread=0;
			shipcolpar1->info.fSizeStart=5;
			shipcolpar1->info.fParticleLifeMin=2;
			shipcolpar1->info.fDirection=180;
			shipcolpar1->info.fRadialAccelMin=20;
			shipcolpar1->info.fRadialAccelMax=30;
			shipcolpar1->info.fGravityMin=0;
			shipcolpar1->info.fGravityMax=0;
			shipcolpar1->Fire();
			if(rand()%3)
				AddBonus(pSpriteHittee->v0.x+10,pSpriteHittee->v0.y+5);
			pSpriteHittee->Kill();
			pSpriteHitter->Kill();
			//爆炸画面
			/*
			SpaceSprite* pSprite = new SpaceSprite(boomtex,28,5,0,0,64,64);
			pSprite->m_bOneCycle=true;
			pSprite->Play();
			pSprite->SetVelocity(0,0);
			pSprite->SetBlendMode(BLEND_COLORMUL);
			pSprite->SetPosition(pSpriteHittee->v0.x+10,pSpriteHittee->v0.y);
			AddSprite(pSprite);
			*/
		}
	}
	else
	{
		//加钱
		if((pHitter==shiptex||pHitter==ship1tex||pHitter==ship2tex||pHitter==ship3tex||pHitter==ship4tex||pHitter==ship5tex)&&pHittee==bonus3tex)
		{
			g_iMoney+=rand()%20;
			pSpriteHittee->Kill();
			if(++i_Dif>DDELAY)
				if(g_iDifficulty>20)
				{
					i_Dif=0;
					g_iDifficulty--;
				}
		}
		//加生命
		if((pHitter==shiptex||pHitter==ship1tex||pHitter==ship2tex||pHitter==ship3tex||pHitter==ship4tex||pHitter==ship5tex)&&pHittee==bonus1tex)
		{
			(g_Ship->iLive)+=rand()%30+5;
			if(++i_Dif>DDELAY)
				if(g_iDifficulty>20)
				{
					i_Dif=0;
					g_iDifficulty--;
				}
				
				pSpriteHittee->Kill();	
		}
		//加许多生命
		if((pHitter==shiptex||pHitter==ship1tex||pHitter==ship2tex||pHitter==ship3tex||pHitter==ship4tex||pHitter==ship5tex)&&pHittee==bonus2tex)
		{
			(g_Ship->iLive)+=rand()%10+10;
			if(++i_Dif>DDELAY)
				if(g_iDifficulty>20)
				{
					i_Dif=0;
					g_iDifficulty--;
				}
				pSpriteHittee->Kill();	
		}
		//加等级
		if((pHitter==shiptex||pHitter==ship1tex||pHitter==ship2tex||pHitter==ship3tex||pHitter==ship4tex||pHitter==ship5tex)&&(pHittee==bonus0tex))
		{
			pSpriteHittee->Kill();
			cur_power++;
			if(++i_Dif>DDELAY)
				if(g_iDifficulty>20)
				{
					i_Dif=0;
					g_iDifficulty--;
				}
		}
	}
	if(((pHitter==shiptex||pHitter==ship1tex||pHitter==ship2tex||pHitter==ship3tex||pHitter==ship4tex||pHitter==ship5tex)&&(pHittee==bonus0tex))||
		(
		(
		pHitter==shipshottex||pHitter==shipshot1tex||pHitter==shipshot2tex||pHitter==shipshot3tex
		)
		&&
		(
		pHittee==enemy0tex||pHittee==enemy1tex||pHittee==enemy2tex||pHittee==enemy3tex||pHittee==enemy4tex||pHittee==enemy5tex||pHittee==enemy6tex
		)
		)
		)
		if(cur_power%5==0)
		{
			(g_Ship->iLevel)++;
			int lselect;
            if((g_Ship->iLevel>20)&&(g_Ship->iLevel<=50))
				lselect=1;
            if((g_Ship->iLevel>400)&&(g_Ship->iLevel<=1000))
				lselect=2;
            if((g_Ship->iLevel>1000)&&(g_Ship->iLevel<=2000))
				lselect=3;
            if((g_Ship->iLevel>2000)&&(g_Ship->iLevel<=3000))
				lselect=4;
            if((g_Ship->iLevel>3000)&&(g_Ship->iLevel<=5000))
				lselect=5;
			//处理变身
			switch(lselect)
			{
			case 1:
				{
					shipbonuspar0->info.nEmission=200;
					shipbonuspar0->info.colColorStart=ARGB(255,255,255,255);
					shipbonuspar0->info.colColorEnd=ARGB(0,0,0,0);
					shipbonuspar0->info.fSpread=0;
					shipbonuspar0->info.fSizeStart=15;
					shipbonuspar0->info.fParticleLifeMin=2;
					shipbonuspar0->info.fDirection=180;
					shipbonuspar0->info.fRadialAccelMin=200;
					shipbonuspar0->info.fRadialAccelMax=300;
					shipbonuspar0->info.fGravityMin=0;
					shipbonuspar0->info.fGravityMax=0;
					shipbonuspar0->Fire();
					g_Ship->SetTexture(ship1tex);
					g_iShotLevel=1;
					if(++i_Dif>DDELAY)
						if(g_iDifficulty>20)
						{
							i_Dif=0;
							g_iDifficulty--;
						}
				}
				break;
			case 2:
				{
					shipbonuspar1->info.nEmission=200;
					shipbonuspar1->info.colColorStart=ARGB(255,255,255,255);
					shipbonuspar1->info.colColorEnd=ARGB(0,0,0,0);
					shipbonuspar1->info.fSpread=0;
					shipbonuspar1->info.fSizeStart=15;
					shipbonuspar1->info.fParticleLifeMin=2;
					shipbonuspar1->info.fDirection=180;
					shipbonuspar1->info.fRadialAccelMin=200;
					shipbonuspar1->info.fRadialAccelMax=300;
					shipbonuspar1->info.fGravityMin=0;
					shipbonuspar1->info.fGravityMax=0;
					shipbonuspar1->Fire();
					g_Ship->SetTexture(ship2tex);
					g_iShotLevel=2;
					if(++i_Dif>DDELAY)
						if(g_iDifficulty>20)
						{
							i_Dif=0;
							g_iDifficulty--;
						}
				}
				break;
			case 3:
				{
					shipbonuspar2->info.nEmission=200;
					shipbonuspar2->info.colColorStart=ARGB(255,255,255,255);
					shipbonuspar2->info.colColorEnd=ARGB(0,0,0,0);
					shipbonuspar2->info.fSpread=0;
					shipbonuspar2->info.fSizeStart=15;
					shipbonuspar2->info.fParticleLifeMin=2;
					shipbonuspar2->info.fDirection=180;
					shipbonuspar2->info.fRadialAccelMin=200;
					shipbonuspar2->info.fRadialAccelMax=300;
					shipbonuspar2->info.fGravityMin=0;
					shipbonuspar2->info.fGravityMax=0;
					shipbonuspar2->Fire();
					g_Ship->SetTexture(ship3tex);
					g_iShotLevel=3;
					if(++i_Dif>DDELAY)
						if(g_iDifficulty>20)
						{
							i_Dif=0;
							g_iDifficulty--;
						}
						
				}
				break;
			case 4:
				{
					shipbonuspar2->info.nEmission=200;
					shipbonuspar2->info.colColorStart=ARGB(255,255,255,255);
					shipbonuspar2->info.colColorEnd=ARGB(0,0,0,0);
					shipbonuspar2->info.fSpread=0;
					shipbonuspar2->info.fSizeStart=15;
					shipbonuspar2->info.fParticleLifeMin=2;
					shipbonuspar2->info.fDirection=180;
					shipbonuspar2->info.fRadialAccelMin=200;
					shipbonuspar2->info.fRadialAccelMax=300;
					shipbonuspar2->info.fGravityMin=0;
					shipbonuspar2->info.fGravityMax=0;
					shipbonuspar2->Fire();
					g_iShotLevel=4;
					if(++i_Dif>DDELAY)
						if(g_iDifficulty>20)
						{
							i_Dif=0;
							g_iDifficulty--;
						}
						
						g_Ship->SetTexture(ship4tex);
				}
				break;
			case 5:
				{
					shipbonuspar3->info.nEmission=200;
					shipbonuspar3->info.colColorStart=ARGB(255,255,230,255);
					shipbonuspar3->info.colColorEnd=ARGB(0,0,0,0);
					shipbonuspar3->info.fSpread=0;
					shipbonuspar3->info.fSizeStart=7;
					shipbonuspar3->info.fParticleLifeMin=0;
					shipbonuspar3->info.fDirection=180;
					shipbonuspar3->info.fRadialAccelMin=20;
					shipbonuspar3->info.fRadialAccelMax=30;
					shipbonuspar3->info.fGravityMin=0;
					shipbonuspar3->info.fGravityMax=0;
					shipbonuspar3->Fire();
					if(++i_Dif>DDELAY)
						if(g_iDifficulty>20)
						{
							i_Dif=0;
							g_iDifficulty--;
						}
						
						g_iShotLevel=5;
						g_Ship->SetTexture(ship5tex);
				}
				break;
				}
	}
	//超大爆炸
	if((pHitter==minetex)
		&&
		(
		pHittee==roid0tex||pHittee==roid1tex||pHittee==roid2tex||
		pHittee==enemy0tex||pHittee==enemy1tex||pHittee==enemy2tex||pHittee==enemy3tex||pHittee==enemy4tex||pHittee==enemy5tex||pHittee==enemy6tex
		))
	{
		shipcolpar1->MoveTo(pSpriteHittee->v0.x+30,pSpriteHittee->v0.y+35);
		shipcolpar1->info.nEmission=50;
		shipcolpar1->info.fSpread=0;
		shipcolpar1->info.fSizeStart=15;
		shipcolpar1->info.fParticleLifeMin=2;
		shipcolpar1->info.fDirection=180;
		shipcolpar1->info.fRadialAccelMin=200;
		shipcolpar1->info.fRadialAccelMax=300;
		shipcolpar1->info.fGravityMin=0;
		shipcolpar1->info.fGravityMax=0;
		shipcolpar1->Fire();
		if(rand()%3)
		{
		 AddBonus(pSpriteHittee->v0.x+10,pSpriteHittee->v0.y+5);
		 AddBonus(pSpriteHittee->v0.x+0,pSpriteHittee->v0.y);
		}
	//	pSpriteHitter->m_bHidden=true;
		pSpriteHittee->Kill();
	}
	return false; 
}
//初始化
void GameStart()
{
	//导入系统
	//子弹水平
	g_iShotLevel=0;
	bgtex=hge->Texture_Load("Picture/space.jpg");
	minetex=hge->Texture_Load("Picture/mine.png");
	snd=hge->Effect_Load("snd/menu.wav");
	//导入我方飞船和子弹
	shipshot1tex=hge->Texture_Load("Picture/tail1.png");
    shipshot2tex=hge->Texture_Load("Picture/tail2.png");
	ship1tex=hge->Texture_Load("Picture/ship1.png");
    ship2tex=hge->Texture_Load("Picture/ship2.png");
    ship3tex=hge->Texture_Load("Picture/ship3.png");
    ship4tex=hge->Texture_Load("Picture/ship4.png");
    ship5tex=hge->Texture_Load("Picture/ship5.png");
	shiptex=hge->Texture_Load("Picture/ship.png");
	shipshottex=hge->Texture_Load("Picture/tail.png");
	//闪电
	shellplaytex=hge->Texture_Load("Picture/shellplay.png");
	//我方粒子系统
	shippartex=hge->Texture_Load("Picture/particles.png");
	shipparsprite=new hgeSprite(shippartex,0,0,32,32);
    bonuspar0sprite=new hgeSprite(shippartex,32,32,32,32);
    bonuspar1sprite=new hgeSprite(shippartex,0,0,32,32);
    bonuspar2sprite=new hgeSprite(shippartex,0,0,32,32);
    bonuspar3sprite=new hgeSprite(shippartex,0,0,32,32);
    bonuspar0sprite->SetBlendMode(BLEND_COLORMUL| BLEND_ALPHAADD | BLEND_NOZWRITE);
	bonuspar0sprite->SetHotSpot(16,16);
	bonuspar1sprite->SetBlendMode(BLEND_COLORMUL| BLEND_ALPHAADD | BLEND_NOZWRITE);
	bonuspar1sprite->SetHotSpot(16,16);
	bonuspar2sprite->SetBlendMode(BLEND_COLORMUL| BLEND_ALPHAADD | BLEND_NOZWRITE);
	bonuspar2sprite->SetHotSpot(16,16);
	bonuspar3sprite->SetBlendMode(BLEND_COLORMUL| BLEND_ALPHAADD | BLEND_NOZWRITE);
	bonuspar3sprite->SetHotSpot(16,16);
	
	shipparsprite->SetBlendMode(BLEND_COLORMUL| BLEND_ALPHAADD | BLEND_NOZWRITE);
	shipparsprite->SetHotSpot(16,16);
	shipbonuspar0=new hgeParticleSystem("Psi/explode1.psi",bonuspar0sprite);
	shipbonuspar1=new hgeParticleSystem("Psi/explode2.psi",bonuspar1sprite);
	shipbonuspar2=new hgeParticleSystem("Psi/explode3.psi",bonuspar2sprite);
	shipbonuspar3=new hgeParticleSystem("Psi/explode4.psi",bonuspar3sprite);
	shippar=new hgeParticleSystem("Psi/particle.psi",shipparsprite);
	shippar->Fire();
	shippar->info.nEmission=15;
	
	shipcolpar0=new hgeParticleSystem("Psi/small_exp.psi",shipparsprite);
    
    shipcolpar1=new hgeParticleSystem("Psi/dead.psi",shipparsprite);
	
	shippar->info.fSizeStart=1;
    shippar->info.fSizeEnd=0;
	shippar->info.colColorStart=ARGB(255,255,230,0);
    shippar->info.fSpeedMin=-90;
    shippar->info.fSpeedMax=-100;
	shippar->info.fGravityMin=-20;
    shippar->info.fGravityMin=-20;
    shippar->info.fDirection=0;
	//导入敌人和其子弹资源
	enemy0tex=hge->Texture_Load("Picture/enemy0.png");
	enemy1tex=hge->Texture_Load("Picture/enemy1.png");
	enemy2tex=hge->Texture_Load("Picture/enemy2.png");
	enemy3tex=hge->Texture_Load("Picture/enemy3.png");
	enemy4tex=hge->Texture_Load("Picture/enemy4.png");
	enemy5tex=hge->Texture_Load("Picture/enemy5.png");
	enemy6tex=hge->Texture_Load("Picture/enemy6.png");
	
	enemyshot1tex=hge->Texture_Load("Picture/enemyshot1.png");
	enemyshot2tex=hge->Texture_Load("Picture/enemyshot2.png");
	shipshot3tex=hge->Texture_Load("Picture/enemyshot2.png");
	enemyshot3tex=hge->Texture_Load("Picture/enemyshot3.png");
	enemyshot4tex=hge->Texture_Load("Picture/enemyshot4.png");
	//导入陨石
	roid0tex=hge->Texture_Load("Picture/roid0.png");
	roid1tex=hge->Texture_Load("Picture/roid1.png");
	roid2tex=hge->Texture_Load("Picture/roid2.png");
    //奖励
    bonus0tex=hge->Texture_Load("Picture/bonus0.png");
    bonus1tex=hge->Texture_Load("Picture/bonus1.png");
    bonus2tex=hge->Texture_Load("Picture/bonus2.png");
    bonus3tex=hge->Texture_Load("Picture/bonus3.png");
    //云
	/*
    cloud0tex=hge->Texture_Load("Picture/cloud0.png");
    cloud1tex=hge->Texture_Load("Picture/cloud1.png");
	cloud2tex=hge->Texture_Load("Picture/cloud2.png");
	cloud3tex=hge->Texture_Load("Picture/cloud3.png");
	cloud4tex=hge->Texture_Load("Picture/cloud4.png");
	cloud5tex=hge->Texture_Load("Picture/cloud5.png");
	cloud6tex=hge->Texture_Load("Picture/cloud6.png");
    cloud7tex=hge->Texture_Load("Picture/cloud7.png");
	cloud8tex=hge->Texture_Load("Picture/cloud8.png");
	cloud9tex=hge->Texture_Load("Picture/cloud9.png");
	*/
	g_iDifficulty=150;
	boomtex=hge->Texture_Load("Picture/explode.png");
	fnt=new hgeFont("Font/font1.fnt");
	if(shiptex&&shipshottex&&bgtex)
	{
		g_Ship=new SpaceSprite(shiptex,4,5,0,0,64,64,300,400);
		bgspr=new hgeSprite(bgtex,0,0,600,500);
		g_Ship->m_baBoundsAction=BA_STOP;
		g_Ship->iLive=700;
		g_Ship->iLevel=0;
		g_iMoney=100;
		AddSprite(g_Ship);
		//控件
		gui=new hgeGUI();
		gui->AddCtrl(new hgeGUIMenuItem(1,fnt,snd,400,200,0.0f,"Play"));
		gui->AddCtrl(new hgeGUIMenuItem(2,fnt,snd,400,240,0.1f,"Options"));
		gui->AddCtrl(new hgeGUIMenuItem(3,fnt,snd,400,280,0.2f,"Instructions"));
		gui->AddCtrl(new hgeGUIMenuItem(4,fnt,snd,400,320,0.3f,"Credits"));
		gui->AddCtrl(new hgeGUIMenuItem(5,fnt,snd,400,360,0.4f,"Exit"));
		
		gui->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
		gui->SetFocus(1);
		gui->Enter();
		hge->System_Start();
	}
	else
	{
		MessageBox(NULL,hge->System_GetErrorMessage(),"Error",MB_OK|MB_APPLMODAL|MB_ICONERROR); 
	}
}
void GameEnd()
{
	hge->Texture_Free(bgtex);
	delete bgspr;
    CleanupSprites();
	hge->System_Shutdown();
	hge->Release();
}
//渲染处理
bool RenderFunc()
{
	hge->Gfx_BeginScene();
	bgspr->Render(0,0);
    //shippar->Render();
	if(shipbonuspar0&&shipbonuspar1&&shipbonuspar2&&shipbonuspar3)
	{
		shipbonuspar0->Render();
        shipbonuspar1->Render();
        shipbonuspar2->Render();
        shipbonuspar3->Render();
	}
	DrawSprites();
    shipcolpar0->Render();
	shipcolpar1->Render();
	if(g_Ship->iLive>=0)
		fnt->printf(5,5,HGETEXT_LEFT,
		"Money:%d     Live:%d   Level:%d",g_iMoney,g_Ship->iLive,g_Ship->iLevel);
	//gui->Render();
	hge->Gfx_EndScene();
	return false;
}
//逻辑处理
bool FrameFunc()
{
	if(g_Ship->iLive<=0)
	{
		MessageBox(NULL,"GameOver","GameOver",MB_OK);
		return true;
	}
	else
	{
		float dt=hge->Timer_GetDelta();
		bool b_return=HandleKeys(dt);
		//控件
#if 0
		int id;
		static int lastid=0;          
		id=gui->Update(dt);
		if(id == -1)
		{
			switch(lastid)
			{
			case 1:
			case 2:
			case 3:
			case 4:
				gui->SetFocus(1);
				gui->Enter();
				break;
				
			case 5: return true;
			}
		}
		else if(id) { lastid=id; gui->Leave(); }
#endif 
		UpdateSprites(dt);
		//更新粒子系统
		shippar->MoveTo(g_Ship->v0.x+34,g_Ship->v0.y+63);
		shippar->Update(dt);
		shipcolpar0->Update(dt);
		shipcolpar1->Update(dt);
		if(shipbonuspar0&&shipbonuspar1&&shipbonuspar2&&shipbonuspar3)
		{
			shipbonuspar0->MoveTo(g_Ship->v0.x+30,g_Ship->v0.y+35);
			shipbonuspar0->Update(dt);
			shipbonuspar1->MoveTo(g_Ship->v0.x+30,g_Ship->v0.y+35);
			shipbonuspar1->Update(dt);
			shipbonuspar2->MoveTo(g_Ship->v0.x+30,g_Ship->v0.y+35);
			shipbonuspar2->Update(dt);
			shipbonuspar3->MoveTo(g_Ship->v0.x+30,g_Ship->v0.y+35);
			shipbonuspar3->Update(dt);
		}
		
		/*
		static float t=0.0f;
		float tx,ty;
		t+=dt;
		tx=-50*cosf(t/60);
		ty=-50*sinf(t/60);
		quad.v[0].tx=tx;        quad.v[0].ty=ty;
		quad.v[1].tx=tx+MYSCREENWIDTH/64; quad.v[1].ty=ty;
		quad.v[2].tx=tx+MYSCREENWIDTH/64; quad.v[2].ty=ty+MYSCREENHEIGHT/64;
		quad.v[3].tx=tx;        quad.v[3].ty=ty+MYSCREENHEIGHT/64;
		*/	
		
		if((rand()%g_iDifficulty)==0)
		{	
	    	AddAlien();
			
			//	AddBonus(); 
		}
		else if(rand()%50==0)
		    AddRoid();
		return b_return;
	}
}
